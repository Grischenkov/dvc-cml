import yaml
import pandas as pd

from sklearn.ensemble import RandomForestClassifier

config = yaml.safe_load(open('params.yaml'))
max_depth = config['random-forest']['max_depth']
random_state = config['random-forest']['random_state']

train = pd.read_csv('./data/train/Iris_train.csv', index_col='Id')
X_train = train.drop(columns=['Species']).to_numpy()
y_train = train['Species'].to_numpy()

test = pd.read_csv('./data/test/Iris_test.csv', index_col='Id')
X_test = test.drop(columns=['Species']).to_numpy()

rfc = RandomForestClassifier(max_depth=max_depth, random_state=random_state).fit(X_train, y_train)
y_pred = rfc.predict(X_test).tolist()

with open('random-forest/predict.txt', 'w') as f:
    for el in y_pred:
        f.write(f"{el} \n")