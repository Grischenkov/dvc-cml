import yaml
import pandas as pd

from sklearn.cluster import KMeans

config = yaml.safe_load(open('params.yaml'))
random_state = config['k-means']['random_state']

train = pd.read_csv('./data/train/Iris_train.csv', index_col='Id')
X_train = train.drop(columns=['Species']).to_numpy()
y_train = train['Species'].to_numpy()

test = pd.read_csv('./data/test/Iris_test.csv', index_col='Id')
X_test = test.drop(columns=['Species']).to_numpy()

kmeans = KMeans(n_clusters=3, random_state=random_state).fit(X_train, y_train)
y_pred = kmeans.predict(X_test).tolist()

with open('k-means/predict.txt', 'w') as f:
    for el in y_pred:
        f.write(f"{el} \n")