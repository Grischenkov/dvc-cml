import yaml
import pandas as pd

from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import train_test_split

df = pd.read_csv('data/Iris.csv', index_col='Id')
df['Species'] = LabelEncoder().fit_transform(df['Species'])

config = yaml.safe_load(open('params.yaml'))
test_size = config['data_split']['test_size']
random_state = config['data_split']['random_state']

df_train, df_test = train_test_split(df, test_size=test_size, random_state=random_state)

df_train.to_csv('data/train/Iris_train.csv')
df_test.to_csv('data/test/Iris_test.csv')