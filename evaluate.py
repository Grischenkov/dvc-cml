import pandas as pd
import matplotlib.pyplot as plt

from sklearn.metrics import accuracy_score

y_test = pd.read_csv('data/test/Iris_test.csv', index_col='Id')['Species'].to_numpy().tolist()

dtree_pred = list()
with open('dtree/predict.txt', 'r') as f:
    for line in f:
        dtree_pred.append(int(line))

kmeans_pred = list()
with open('k-means/predict.txt', 'r') as f:
    for line in f:
        kmeans_pred.append(int(line))

randomforest_pred = list()
with open('random-forest/predict.txt', 'r') as f:
    for line in f:
        randomforest_pred.append(int(line))

dtree_report = accuracy_score(y_test, dtree_pred)
kmeans_report = accuracy_score(y_test, kmeans_pred)
randomforest_report = accuracy_score(y_test, randomforest_pred)

with open('metrics.txt', 'w') as f:
    f.write(f"dtree acc: {dtree_report}")
    f.write("\n")
    f.write(f"k-means acc: {kmeans_report}")
    f.write("\n")
    f.write(f"random-forest acc: {randomforest_report}")

f, ax = plt.subplots()
ax.bar(['dtree', 'k-means', 'random-forest'], [dtree_report, kmeans_report, randomforest_report])
f.savefig('plot.png')